<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class API extends Controller
{
    public function getTop3() {
        $autos = DB::select(
            'call select_top_3_cars()'
          );

        $autos = json_decode(json_encode($autos), true);
        $x = 0;

        foreach ($autos as $auto) {
            $auto['Marke'] = DB::table('tabmarke')->where('IDMarke', $auto['IDMarke'])->value('Name');
            $auto['Antrieb'] = DB::table('tabantrieb')->where('IDAntrieb', $auto['IDAntrieb'])->value('Name');
            $auto['Aufbauart'] = DB::table('tabaufbauart')->where('IDAufbauart', $auto['IDAufbauart'])->value('Name');
            $autos[$x] = $auto;
            $x++;
        }

        return $autos;
    }

    public function getAll() {
        $autos = DB::table('tabfahrzeug')->get()->toArray();
        $autos = json_decode(json_encode($autos), true);
        $x = 0;
        foreach ($autos as $auto) {
            $auto['Marke'] = DB::table('tabmarke')->where('IDMarke', $auto['IDMarke'])->value('Name');
            $auto['Antrieb'] = DB::table('tabantrieb')->where('IDAntrieb', $auto['IDAntrieb'])->value('Name');
            $auto['Aufbauart'] = DB::table('tabaufbauart')->where('IDAufbauart', $auto['IDAufbauart'])->value('Name');
            $autos[$x] = $auto;
            $x++;
        }

        return $autos;
    }

    public function getCar($IDFahrzeug) {
        $Fahrzeug = DB::table('tabfahrzeug')->where('IDFahrzeug', $IDFahrzeug)->first();
        $Fahrzeug = json_decode(json_encode($Fahrzeug), true);
        $Fahrzeug['Marke'] = DB::table('tabmarke')->where('IDMarke', $Fahrzeug['IDMarke'])->value('Name');
        $Fahrzeug['Antrieb'] = DB::table('tabantrieb')->where('IDAntrieb', $Fahrzeug['IDAntrieb'])->value('Name');
        $Fahrzeug['Aufbauart'] = DB::table('tabaufbauart')->where('IDAufbauart', $Fahrzeug['IDAufbauart'])->value('Name');

        $User = DB::table('tabuser')->where('IDUser', $Fahrzeug['IDUser'])->first();
        $User = json_decode(json_encode($User), true);
        $Fahrzeug['Vorname'] = $User['Vorname'];
        $Fahrzeug['Nachname'] = $User['Nachname'];
        $Fahrzeug['Geburtstag'] = $User['Geburtstag'];
        $Fahrzeug['PLZ'] = $User['PLZ'];
        $Fahrzeug['Ort'] = $User['Ort'];
        $Fahrzeug['Email'] = $User['Email'];
        $Fahrzeug['Handynummer'] = $User['Handynummer'];

        return $Fahrzeug;
    }

    public function login(Request $request) {
      $request = (json_decode(request()->getContent(), true))['user'];
        $username = $request['username'];
        $passwort = $request['password'];
        $passwortSalty = $passwort . "carTrader";
        $passwortHashed = hash('md5', $passwortSalty);

        if (DB::table('tabuser')->where('username', $username)->exists()) {
            if (DB::table('tabuser')->where('username', $username)->where('Passwort', $passwortHashed)->exists()) {
                $id = DB::table('tabuser')->where('username', $username)->value('idUser');
                $berechtigung = DB::table('tabuser')->where('username', $username)->value('berechtigung');
                session(['IDUser' => $id]);
                session(['Berechtigung' => $berechtigung]);
                return $berechtigung;
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function register(Request $request) {
      $request = (json_decode(request()->getContent(), true))['form'];

        $passwort = $request['password'];
        $passwortSalty = $passwort . "carTrader";
        $passwortHashed = hash('md5', $passwortSalty);

        $vorname = $request['fname'];
        $nachname = $request['lname'];
        $username = $request['username'];

        // try {
          DB::table('tabuser')->insert([
            'passwort' => $passwortHashed,
            'vorname' => $vorname,
            'nachname' => $nachname,
            'username' => $username
          ]);
          return 1;
        // } catch {
        //   return 0;
        // }
    }

    public function create(Request $request) {
        //if ($request->session()->has('IDUser')) {
        $request = (json_decode(request()->getContent(), true))['form'];

            $Modell = $request['Modell'];
            $Marke = $request['Marke'];
            $Version = $request['Version'];
            $PS = $request['PS'];
            $Zylinder = $request['Zylinder'];
            $Hubraum = $request['Hubraum'];
            $Leergewicht = $request['Leergewicht'];
            $Aufbauart = $request['Aufbauart'];
            $Antrieb = $request['Antrieb'];
            $Preis = $request['Preis'];
            $IDUser = 2;

            $IDMarke = DB::table('tabmarke')->where('name', $Marke)->value('IDMarke');
            $IDAufbauart = DB::table('tabaufbauart')->where('name', $Aufbauart)->value('IDAufbauart');
            $IDAntrieb = DB::table('tabantrieb')->where('name', $Antrieb)->value('IDAntrieb');

            DB::table('tabfahrzeug')->insert([
                'IDUser' => $IDUser,
                'IDMarke' => $IDMarke,
                'IDAufbauart' => $IDAufbauart,
                'IDAntrieb' => $IDAntrieb,
                'Modell' => $Modell,
                'PS' => $PS,
                'Zylinder' => $Zylinder,
                'Hubraum' => $Hubraum,
                'Leergewicht' => $Leergewicht,
                'Version' => $Version,
                'Preis' => $Preis
            ]);

            return 1;
        //} else {
            //return 0;
        //}
    }
}
