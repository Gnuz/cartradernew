<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'API@getAll');
Route::get('/car/{IDFahrzeug}', 'API@getCar');
Route::post('/login', 'API@login');
Route::post('/register', 'API@register');
Route::post('/create', 'API@create');
