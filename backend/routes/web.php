<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'API@getAll');
Route::get('/top3', 'API@getTop3');
Route::get('/car/{IDFahrzeug}', 'API@getCar');
Route::post('/login', 'API@login');
Route::post('/register', 'API@register');
Route::post('/create', 'API@create');
