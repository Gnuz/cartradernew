import { Component, OnInit } from '@angular/core';
import { CarService } from '../service/car.service';
import { Car } from '../data/car';
import { Observable } from 'rxjs';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  isLoading: Boolean = true;
  carList: Car[] = [];

  constructor(private carService: CarService) { }

  ngOnInit() {
    this.displayCars();
  }

  async displayCars(){
    await this.carService.getCars()
    .then((data) => {
      this.carList = data;
      this.isLoading = false;
      console.log(this.carList);
    });
    console.log(this.carList.length);
  }


}
