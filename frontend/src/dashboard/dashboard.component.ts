import { Component, OnInit } from '@angular/core';
import { CarService } from '../service/car.service';
import { Car } from '../data/car';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private carService: CarService) { }

  topCarList: Car[] = [];

  ngOnInit() {
    this.displayCars();
  }

  async displayCars(){
    await this.carService.getTopCars()
    .then((data) => {
      this.topCarList = data;
      console.log(this.topCarList);
    });
    console.log(this.topCarList.length);
  }

}
