import { Component, OnInit, ViewChild } from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { ClrLoadingState } from '@clr/angular';
import { CarService } from '../service/car.service';
import { Car } from '../data/car';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  uploadForm: FormGroup = new FormGroup({
    Marke: new FormControl('', [
      Validators.required,
      Validators.minLength(2)
    ]),
    Modell: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    Version: new FormControl('', [
      Validators.required
    ]),
    PS: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    Zylinder: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    Hubraum: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    Leergewicht: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    Aufbauart: new FormControl('Sedan', [
      Validators.required,
    ]),
    Antrieb: new FormControl('Front wheel drive', [
      Validators.required,
    ]),
    Preis: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ])
  });

  uploadBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;

  constructor(private carService: CarService) { }

  ngOnInit() {
  }

  onSubmit(){
    this.uploadBtnState = ClrLoadingState.LOADING;
    this.carService.uploadCar(this.uploadForm.value);

    /*let car: Car = new Car(
      this.uploadForm.get('Marke').value,
      this.uploadForm.get('Modell').value,
      this.uploadForm.get('Version').value,
      this.uploadForm.get('PS').value,
      this.uploadForm.get('Zylinder').value,
      this.uploadForm.get('Hubraum').value,
      this.uploadForm.get('Leergewicht').value,
      this.uploadForm.get('Aufbauart').value,
      this.uploadForm.get('Antrieb').value,
      this.uploadForm.get('Preis').value*/
    //);
  }

}
