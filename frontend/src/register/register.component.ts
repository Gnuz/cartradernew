import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { ClrLoadingState } from '@clr/angular';
import { RegisterUser } from '../data/register-user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup = new FormGroup({
    fname: new FormControl('', [
      Validators.required,
    ]),
    lname: new FormControl('', [
      Validators.required,
    ]),
    username: new FormControl('', [
      Validators.required,
    ]),
    password: new FormControl('', [
      Validators.required,
    ]),
  });
  loginBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  async register(){
    this.userService.register(this.registerForm.value);
  }



}
