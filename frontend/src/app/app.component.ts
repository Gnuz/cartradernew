import { Component, OnInit } from '@angular/core';
import { User } from '../data/user';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  title = 'CarTrader';
  userCheck: String;

  ngOnInit(): void {
    this.userCheck = '<?php echo $_SESSION["berechtigung"]>'

    let uploadLink = document.getElementById('upload');

    if(this.userCheck == ''){
      uploadLink.setAttribute("color", "green");
    }else if(this.userCheck == '1'){
      uploadLink.setAttribute("color", "blue");
    }
      
  }
}
