import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../data/user';
import {FormGroup, FormControl, Validators} from "@angular/forms";

const urlServer = 'http://127.0.0.1:8000';
const httpHeaders =  new HttpHeaders({
  'Content-Type':  'application/json',
});

@Injectable({
  providedIn: 'root'
})
export class UserService {

constructor(private http: HttpClient) { }

async login(user: User): Promise<Number> {
  const data = JSON.stringify({user});
  return await this.http.post<Number>(urlServer + '/login', data, { headers: httpHeaders })
  .toPromise();
}

async register(form: FormGroup) {
  const data = JSON.stringify({form});
  console.log(data);
  return await this.http.post(urlServer + '/register', data, { headers: httpHeaders })
  .toPromise();
}
}
