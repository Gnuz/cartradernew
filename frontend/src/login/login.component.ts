import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { ClrLoadingState } from '@clr/angular';
import { UserService } from '../service/user.service';
import { User } from 'src/data/user';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [
      Validators.required,
    ]),
    password: new FormControl('', [
      Validators.required,
    ]),
  });

  loginBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;

  constructor(private userService: UserService) { }

  newUser: User;
  userCheck: Number;

  ngOnInit() {

  }

  async login(){
    this.loginBtnState = ClrLoadingState.LOADING;

    this.userService.login(this.loginForm.value)
    .then((data) => {
      this.userCheck = data;
      console.log("Is logged in: " + this.userCheck)
    });

    if(this.userCheck = 1){
      alert('Logged in successfully');
    }else if(this.userCheck = 2){
      alert('Logged in successfully as admin');
    }else{
      alert('Wrong username or password');
    }

  }

}
