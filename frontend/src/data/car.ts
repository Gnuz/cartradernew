export class Car{

    Marke: String;
    Modell: String;
    Version: String;
    PS: Number;
    Zylinder: Number;
    Hubraum: Number;
    Leergewicht: Number;
    Aufbauart: String;
    Antrieb: String;
    Preis: Number;

    carList: Car[];
    topCarList: Car[];

    construtor(Marke: String, Modell: String, Version: String, PS: Number, Zylinder: Number, Hubraum: Number, Leergewicht: Number, Aufbauart: String, Antrieb: String, Preis: Number) {
      this.Marke = Marke;
      this.Modell = Modell;
      this.Version = Version;
      this.PS = PS;
      this.Zylinder = Zylinder;
      this.Hubraum = Hubraum;
      this.Leergewicht = Leergewicht;
      this.Aufbauart = Aufbauart;
      this.Antrieb = Antrieb;
      this.Preis = Preis;
    }


}
