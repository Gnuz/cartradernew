export class RegisterUser {
  fname: String;
  lname: String;
  username: String;
  password: String;

  loggedIn: Number;

  public constructor(username: String, password: String, fname: String, lname: String){
      this.username = username;
      this.password = password;
      this.fname = fname;
      this.lname = lname;
  }
}
