-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: cartrader
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tabfahrzeug`
--

DROP TABLE IF EXISTS `tabfahrzeug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tabfahrzeug` (
  `IDFahrzeug` int(11) NOT NULL AUTO_INCREMENT,
  `Modell` varchar(45) DEFAULT NULL,
  `PS` int(4) DEFAULT NULL,
  `Leergewicht` int(5) DEFAULT NULL,
  `IDAntrieb` int(11) DEFAULT NULL,
  `IDUser` int(11) DEFAULT NULL,
  `IDMarke` int(11) DEFAULT NULL,
  `Version` varchar(45) DEFAULT NULL,
  `Zylinder` varchar(45) DEFAULT NULL,
  `Preis` int(10) DEFAULT NULL,
  `IDAufbauart` int(11) DEFAULT NULL,
  `bild` blob,
  `Hubraum` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`IDFahrzeug`),
  UNIQUE KEY `idFahrzeug_UNIQUE` (`IDFahrzeug`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabfahrzeug`
--

LOCK TABLES `tabfahrzeug` WRITE;
/*!40000 ALTER TABLE `tabfahrzeug` DISABLE KEYS */;
INSERT INTO `tabfahrzeug` VALUES (1,'488',670,1000,0,2,2,NULL,NULL,NULL,NULL,NULL,NULL),(2,'345',455,800,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(3,'modell2',500,7,NULL,5,1,NULL,'3',15000,NULL,NULL,'6'),(4,'modell2',500,7,NULL,5,1,NULL,'3',15000,NULL,NULL,'6'),(5,'1',3,6,NULL,2,2,'2','4',7,NULL,NULL,'5'),(6,'1',3,6,NULL,2,2,'2','4',7,NULL,NULL,'5'),(7,'1',1,1,NULL,2,NULL,'111','1',1,NULL,NULL,'1');
/*!40000 ALTER TABLE `tabfahrzeug` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-07 20:33:17
